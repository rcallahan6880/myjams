package com.example.android.myjams;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;


import static com.example.android.myjams.HickHop.KEY_ALBUM;
import static com.example.android.myjams.HickHop.KEY_ARTIST;
import static com.example.android.myjams.HickHop.KEY_SONG;


public class NowPlaying extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        setContentView(R.layout.now_playing);
        String playingSong = "";
        String playingArtist = "";
//        String playingAlbum = "";
//        get list from class
        Intent intent = getIntent();
        if (null != intent) {
            playingSong = intent.getStringExtra(KEY_SONG);
            playingArtist = intent.getStringExtra(KEY_ARTIST);
//            playingAlbum = intent.getStringExtra(KEY_ALBUM);
        }
// Find the ImageView in the list_item.xml layout with the ID list_item_icon
//        ImageView iconView =  findViewById(R.id.album_cover);
//        // Get the image resource ID from the current AndroidFlavor object and
//        // set the image to iconView
//        iconView.setImageResource(playingAlbum);
//         get album cover image
        int album = intent.getIntExtra(KEY_ALBUM, 2131165268);
        ImageView currentAlbum = findViewById(R.id.current_album);
//        currentAlbum.setImageDrawable(album);
        currentAlbum.setImageResource(album);
        // current song
        TextView currentSong = findViewById(R.id.current_song);
        currentSong.setText(playingSong);
//get current artist
        TextView current_artist = findViewById(R.id.current_artist);
        current_artist.setText(playingArtist);

//        final Button playButton =  findViewById(R.id.play_button);


    }
}

