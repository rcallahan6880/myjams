package com.example.android.myjams;

import android.media.Image;

public class Music {
    private String mArtist;

    private String mSong;

    // Drawable resource ID
    private int mImageResourceId;

    public Music(int imageResourceId,String artist, String song){
        mArtist = artist;
        mSong = song;
        mImageResourceId = imageResourceId;
    }

    /**
     *
     * get album image
     */
    public int getImageResourceId() {
        return mImageResourceId;
    }
    /**
     * get Genres
     */
    public String getArtist(){
        return  mArtist;
    }

    /**
     * get Artist
     */
    public String getSong(){
        return mSong;
    }


}
