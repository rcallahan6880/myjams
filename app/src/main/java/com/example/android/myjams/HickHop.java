package com.example.android.myjams;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class HickHop extends AppCompatActivity {
    public static final String KEY_SONG = "KEY_SONG";
    public static final String KEY_ARTIST = "KEY_ARTIST";
    public static final String KEY_ALBUM = "KEY_ALBUM";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.music_list);

        // Create a list of words
        ArrayList<Music> music = new ArrayList<>();

        music.add(new Music(R.drawable.americanrebelution, "The Lacs:", "American Rebelution"));
        music.add(new Music(R.drawable.americanrebelution, "The Lacs:", "Drink As A Team"));
        music.add(new Music(R.drawable.countryboysparadise, "The Lacs:", "Kicking Up Mud"));
        music.add(new Music(R.drawable.workreleaseprogram, "Twang and Round:", "Down in Kentucky"));
        music.add(new Music(R.drawable.pardoned, "Twang and Round:", "Raised On Cornbread (feat. Bottleneck)"));


        // Create an {@link ArrayAdapter}, whose data source is a list of Strings. The
        // adapter knows how to create layouts for each item in the list, using the
        // simple_list_item_1.xml layout resource defined in the Android framework.
        // This list item layout contains a single {@link TextView}, which the adapter will set to
        // display a single word.
        final ListAdapter adapter = new ListAdapter(this, music);

        // Find the {@link ListView} object in the view hierarchy of the {@link Activity}.
        // There should be a {@link ListView} with the view ID called list, which is declared in the
        // word_list.xml file.
        ListView listView =  findViewById(R.id.list);

        // Make the {@link ListView} use the {@link ArrayAdapter} we created above, so that the
        // {@link ListView} will display list items for each word in the list of words.
        // Do this by calling the setAdapter method on the {@link ListView} object and pass in
        // 1 argument, which is the {@link ArrayAdapter} with the variable name itemsAdapter.
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Music item = adapter.getItem(position);
                Intent nowPlaying = new Intent(getApplicationContext(), NowPlaying.class);
                nowPlaying.putExtra(KEY_SONG, item.getSong());
                nowPlaying.putExtra(KEY_ARTIST, item.getArtist());
                nowPlaying.putExtra(KEY_ALBUM,item.getImageResourceId());
//                nowPlaying.putExtra(String.valueOf(KEY_AlBUM), item.getImageResourceId());
                startActivity(nowPlaying);

            }
        });

    }



}
